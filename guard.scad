/**
 * SKIM-2 Cover/Guard
 * Copyright (C) 2024 Ben White
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Dimensions
h_thickness = 2.5; // mm
v_thickness = 1.5; // mm

table_top_length = 41; // mm
table_top_width = 33; // mm
table_leg_height = 20 - v_thickness; // mm

// Hexagon grid
across_corners = 6; // Hexagon size
spacing = 1.5; // Spacing between hexagons
rows = 5; // Number of rows
columns = 6; // Number of columns

// Create table legs
module table_leg() {
    translate([0, 0, -table_leg_height/2])
        cube([h_thickness, h_thickness, table_leg_height]);
}

module table() {
    // Create table top
    translate([-table_top_length/2, -table_top_width/2, 0])
    cube([table_top_length, table_top_width, v_thickness]);
    
    translate([-table_top_length/2, -table_top_width/2, -table_leg_height/2])
    table_leg();

    translate([table_top_length/2 - h_thickness, -table_top_width/2, -table_leg_height/2])
    table_leg();

    translate([-table_top_length/2, table_top_width/2 - h_thickness, -table_leg_height/2])
    table_leg();

    translate([table_top_length/2 - h_thickness, table_top_width/2 - h_thickness, -table_leg_height/2])
    table_leg();
}

module hexagon_grid() {
    // Calculate the spacing between hexagons
    x_spacing = sqrt(3) * across_corners / 2 + spacing;
    y_spacing = 3/2     * across_corners / 2 + spacing;

    // Arrange the hexagons in a grid
    translate([0, 0, -v_thickness/2])
    for (i = [1-columns:columns-1]) {
        for (j = [1-rows:rows-1]) {
            x_offset = i * x_spacing + (j % 2) * (x_spacing / 2);
            y_offset = j * y_spacing;
            
            translate([x_offset, y_offset, 0])
            rotate([0, 0, 90])
            cylinder(d = across_corners, h = v_thickness*2, $fn = 6);
        }
    }
}

difference() {
    table();

    intersection() {
        hexagon_grid();
        
        translate([0, 0, v_thickness/2])
        cube([table_top_length-h_thickness*2, table_top_width-h_thickness*2, v_thickness*2], center=true);
    }
}