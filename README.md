# SKIM-2 Cover/Guard

Provided in the hope it will be useful to someone.

Fits the AllPondSolutions SKIM-2, and similar skimmers sold under different benad names (e.g. Superfish)

![In tank photo](/doc/printed0.jpg)
![Out of tank photo](/doc/printed1.jpg)
